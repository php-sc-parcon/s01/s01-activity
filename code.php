<?php 

function getLetterGrade($grade) {
	if($grade >= 98 && $grade <=100) {
		return 'Is equivalent to A+';

	} else if($grade >= 95 && $grade <=97) {

		return 'Is equivalent to A';

	} else if($grade >= 92 && $grade <=94){

		return 'Is equivalent to A-';

	} else if($grade >= 89 && $grade <=91){

		return 'Is equivalent to B+';

	} else if($grade >= 86 && $grade <=88){

		return 'Is equivalent to B';

	} else if($grade >= 83 && $grade <=85){

		return 'Is equivalent to B-';

	} else if($grade >= 80 && $grade <=82){

		return 'Is equivalent to C+';

	} else if($grade >= 77 && $grade <=79){

		return 'Is equivalent to C';

	} else if($grade >= 75 && $grade <=76){

		return 'Is equivalent to C-';

	} else if($grade <= 74){

		return 'Is equivalent to D';

	} else {
		return 'Not applicable in Grading System!';
	}
}